package ru.konorkin.ivan.android2048.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GameActivity extends ActionBarActivity {
    private static final int INIT_NUMB_OF_CELLS = 2;
    private static final int GRID_SIZE          = 4;
    private static final int BG_SIZE            = 460;
    public static final int BORDER_SIZE         = 13;
    private static int WINDOW_WIDTH;
    private static int WINDOW_HEIGHT;
    public static String PACKAGE_NAME;
    public static GameActivity game;

    Cell cells[][] = new Cell[GRID_SIZE][GRID_SIZE];
    int score = 0;
    RelativeLayout layout;

    void initGame() {
        initConstants();

        for (int i = INIT_NUMB_OF_CELLS; i > 0; --i) {
            addRandomCell();
        }

        updateScore();

        layout.setOnTouchListener(new OnSwipeTouchListener(GameActivity.this) {
            public void onSwipeTop() {
                makeMove(Direction.TOP);
            }
            public void onSwipeRight() {
                makeMove(Direction.RIGHT);
            }
            public void onSwipeLeft() {
                makeMove(Direction.LEFT);
            }
            public void onSwipeBottom() {
                makeMove(Direction.BOTTOM);
            }
        });
    }

    private void makeMove(Direction dir) {
        boolean smt_moved = false;

        int start_x = 0, start_y = 0;
        int end_x = GRID_SIZE, end_y = GRID_SIZE;
        int inc_x = 0, inc_y = 0;

        switch (dir) {
            case BOTTOM:
                start_x = GRID_SIZE - 1;
                end_x = -1;
                inc_x = -1;
                break;
            case TOP:
                inc_x = 1;
                break;
            case LEFT:
                inc_y = 1;
                break;
            case RIGHT:
                start_y = GRID_SIZE - 1;
                end_y = -1;
                inc_y = -1;
                break;
        }

        int last_x = -1, last_y = -1;
        int place_x = start_x, place_y = start_y;

        for (int x = start_x, y = start_y; x != end_x && y != end_y; x += inc_x, y += inc_y) {
            if (cells[x][y] != null) {
                if (cells[place_x][place_y] == null) {
                    cells[place_x][place_y] = cells[x][y];
                    cells[x][y] = null;
                    last_x = x;
                    last_y = y;
                } else {
                    if (cells[place_x][place_y].numb == cells[x][y].numb) {
                        if (last_x != -1) {
                            cells[x][y].move(dir, Math.abs(place_x - x) + Math.abs(place_y - y), ActionAfterMove.DELETE);
                            cells[place_x][place_y].move(dir, Math.abs(place_x - last_x) + Math.abs(place_y - last_y), ActionAfterMove.MULTIPLY);
                            cells[x][y] = null;
                            smt_moved = true;
                            last_x = -1;
                            last_y = -1;
                            place_x += inc_x;
                            place_y += inc_y;
                        } else {
                            last_x = x;
                            last_y = y;
                        }
                    } else {
                        cells[place_x][place_y].move(dir, Math.abs(place_x - last_x) + Math.abs(place_y - last_y), ActionAfterMove.NONE);
                        place_x += inc_x;
                        place_y += inc_y;
                        if (place_x != x || place_y != y) {
                            cells[place_x][place_y] = cells[x][y];
                            cells[x][y] = null;
                            smt_moved = true;
                        }
                        last_x = x;
                        last_y = y;
                    }
                }
            }

            if (x + inc_x == end_x || y + inc_y == end_y) {
                if (last_x != -1) {
                    int move_length = Math.abs(place_x - last_x) + Math.abs(place_y - last_y);
                    cells[place_x][place_y].move(dir, move_length, ActionAfterMove.NONE);
                    last_x = -1;
                    last_y = -1;
                    if (move_length > 0) smt_moved = true;
                }

                if (inc_x == 0) {
                    place_x = ++x;
                    place_y = start_y;
                    y = start_y - inc_y;
                } else {
                    place_y = ++y;
                    place_x = start_x;
                    x = start_x - inc_x;
                }

            }
        }

        if (smt_moved) {
            addRandomCell();
        }

    }

    public void deleteCell(Cell cell) {
        this.layout.removeView(cell);
    }

    private void initConstants() {
        Cell.context = GameActivity.this;
        PACKAGE_NAME = getApplicationContext().getPackageName();
        game = this;
        layout = (RelativeLayout) findViewById(R.id.game_layout);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        WINDOW_HEIGHT = metrics.heightPixels;
        WINDOW_WIDTH = metrics.widthPixels;
    }

    void updateScore() {
        TextView score_tv = (TextView) findViewById(R.id.score);
        score_tv.setText(Integer.toString(score));
    }

    public void incScore(int numb) {
        score += numb;
        updateScore();
    }

    void addRandomCell() {
        int x, y;
        do {
            x = (int) (Math.random() * GRID_SIZE);
            y = (int) (Math.random() * GRID_SIZE);
        } while (cells[x][y] != null);

        cells[x][y] = new Cell(2 * (int) (1 + Math.random() * 2));

        layout.addView(cells[x][y], getParams(x, y));
    }

    private RelativeLayout.LayoutParams getParams(int x, int y) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(Cell.SIZE, Cell.SIZE);
        params.leftMargin = (WINDOW_WIDTH - BG_SIZE) / 2 + y * (Cell.EMPTY_SIZE + BORDER_SIZE) + BORDER_SIZE - 12;
        params.topMargin = (WINDOW_HEIGHT - BG_SIZE) / 2 + x * (Cell.EMPTY_SIZE + BORDER_SIZE) + BORDER_SIZE - 66;
        return params;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
        initGame();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
