package ru.konorkin.ivan.android2048.app;

import android.content.Context;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class Cell extends ImageView {
    public static int SIZE = 98;
    public static int EMPTY_SIZE = 94;
    public static Context context;
    public static long ANIMATION_TIME = 200;

    public int numb;
    int start_x = 0;
    int start_y = 0;

    public Cell(int numb) {
        super(context);
        this.numb = numb;
        initialUpdateImg();
    }

    private void initialUpdateImg() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                updateImg();
            }
        }, ANIMATION_TIME*2);
    }

    public void updateImg() {
        setImageResource(getResources().getIdentifier("cell_" + numb, "drawable", GameActivity.PACKAGE_NAME));
    }

    public void move(Direction dir, int numb_of_cells, final ActionAfterMove action) {
        final Cell cell = this;
        int offset_x = 0, offset_y = 0;
        switch (dir) {
            case LEFT:
                offset_x = -(EMPTY_SIZE + GameActivity.BORDER_SIZE) * numb_of_cells;
                break;
            case RIGHT:
                offset_x = (EMPTY_SIZE + GameActivity.BORDER_SIZE) * numb_of_cells;
                break;
            case TOP:
                offset_y = -(EMPTY_SIZE + GameActivity.BORDER_SIZE) * numb_of_cells;
                break;
            case BOTTOM:
                offset_y = (EMPTY_SIZE + GameActivity.BORDER_SIZE) * numb_of_cells;
                break;
        }
        TranslateAnimation anim = new TranslateAnimation(start_x, start_x + offset_x, start_y, start_y + offset_y);
        start_x += offset_x;
        start_y += offset_y;
        anim.setDuration(ANIMATION_TIME);
        anim.setFillAfter(true);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                switch (action) {
                    case NONE:
                        break;
                    case DELETE:
                        new Handler().post(new Runnable() {
                            public void run() {
                                GameActivity.game.deleteCell(cell);
                            }
                        });
                        break;
                    case MULTIPLY:
                        numb *= 2;
                        updateImg();
                        GameActivity.game.incScore(numb);
                        break;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(anim);
    }
}
