package ru.konorkin.ivan.android2048.app;

public enum ActionAfterMove {
    NONE,
    MULTIPLY,
    DELETE
}
