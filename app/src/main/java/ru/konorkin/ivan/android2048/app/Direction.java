package ru.konorkin.ivan.android2048.app;

public enum Direction {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
}
